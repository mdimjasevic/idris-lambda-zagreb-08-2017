import Data.Vect

my_zip : Vect n a -> Vect n b -> Vect n (a, b)
