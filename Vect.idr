data Vect : Nat -> Type -> Type where
     Nil : Vect Z a
     (::) : a -> Vect n a -> Vect (S n) a

v1 : Vect 0 Int
v1 = []

v2 : Vect 3 String
v2 = ["Ivica", "Perica", "Marica"]
