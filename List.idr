data ListM a = Nil | (::) a (ListM a)

l1 : ListM String
l1 = Nil

l2 : ListM String
l2 = "Ivica" :: "Perica" :: "Marica" :: Nil

l3 : ListM String
l3 = ["Ivica", "Perica", "Marica"]
