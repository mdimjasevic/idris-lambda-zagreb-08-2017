Ovo je prezentacija "Precizni tipovi u Idrisu" koja je održana
9. kolovoza 2017. u tvrtki Five u Zagrebu.

Izvorni kod prezentacije je u datoteci prezentacija.org, a kompilirana
verzija je u prezentacija.html.

© Marko Dimjašević, 2017., CC-BY 4.0, https://dimjasevic.net/marko
