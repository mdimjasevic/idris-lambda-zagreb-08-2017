import Data.Vect

-- Pogledati sa :doc sljedeće:
--   Data.Vect.zip
--   Refl
--   Dec
--   DecEq (sučelje)
--   decEq (funkcija)

total
safeZip : Vect n a -> Vect m b -> Maybe (Vect n (a, b))
safeZip {n} {m} xs ys = case decEq n m of
                             Yes Refl => Just (zip xs ys)   -- Data.Vect.zip iz Idrisa
                             No contra => Nothing
