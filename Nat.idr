-- 'NatM', a ne 'Nat' zbog ugrađenog tipa

data NatM = Z | S NatM

x : NatM
x = Z

y : NatM
y = S (S x)   -- 2
