def fun(x, n):
    if isinstance(x, int) and isinstance(n, int):
        if x >= 0 and x < n:
            # do stuff
        else:
            raise ValueError("Not less than " + str(n))
    else:
        raise TypeError("Not an int")
