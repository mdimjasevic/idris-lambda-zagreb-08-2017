StringOrDouble : (isStr : Bool) -> Type
StringOrDouble False = Double
StringOrDouble True = String

lengthOrDouble : (isStr : Bool) -> StringOrDouble isStr -> Double
lengthOrDouble False x = x + x
lengthOrDouble True x = cast (length x)
